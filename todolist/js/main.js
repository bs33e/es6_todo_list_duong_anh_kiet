import { findIndex, getNewTask, renderTable } from "./controllers.js";
import { ToDoList } from "./models.js";

const BASE_URL = "https://62f8b754e0564480352bf3c3.mockapi.io";
let checkList;

let renderTableToDo = () => {
  axios({
    url: `${BASE_URL}/todo`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      checkList = res.data;
      renderTable(checkList);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

renderTableToDo();

let addToDo = () => {
  let { content, id } = getNewTask();
  let newData = new ToDoList(content, id);
  document.getElementById("newTask").value = "";

  axios({
    url: `${BASE_URL}/todo`,
    method: "POST",
    data: newData,
  })
    .then((res) => {
      checkList.push(newData);

      renderTable(checkList);
    })
    .catch((err) => {});
};
window.addToDo = addToDo;

let updateTask = (dataUpdate) => {
  axios({
    url: `${BASE_URL}/todo/${dataUpdate.id}`,
    method: "PUT",
    data: dataUpdate,
  })
    .then((res) => {
      renderTableToDo();

    })
    .catch((err) => {});
};
window.updateTask = updateTask;


let updateArr = (arrNew) => {
  axios({
    url: `${BASE_URL}/todo`,
    method: "POST",
    data: arrNew,
  })
    .then((res) => {
    
      
    })
    .catch((err) => {});
};

window.updateArr = updateArr;


let addToComplete = (id) => {
  let index = findIndex(id, checkList);
  let taskComplete = checkList[index];
  let checkid = "checkBox" + id;
  

 

  if (document.getElementById(checkid).checked) {
    taskComplete.check = 1;
    updateTask(taskComplete);
  } else {
    taskComplete.check = 0;
    updateTask(taskComplete);
  }
};

window.addToComplete = addToComplete;

let deleteTask = (id) => {
  axios({
    url: `${BASE_URL}/todo/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderTableToDo();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.deleteTask = deleteTask;

let sapxepTang = () => {
  let mangTang = checkList.sort((taskNext, task) => {
    let contentNext = taskNext.content.toLowerCase();
   
    let content = task.content.toLowerCase();
    
    if (contentNext > content) {
      return 1; // Giữ nguyên
    }
    if (contentNext < content) {
      return -1; // Đảo vị trí
    }
    return 1;
  });

  renderTable(mangTang);

  


};
window.sapxepTang = sapxepTang;

let sapxepGiam = () => {
  let mangGiam = checkList.sort((taskNext, task) => {
    let contentNext = taskNext.content.toLowerCase();
    let content = task.content.toLowerCase();
    if (contentNext > content) {
      return -1; // Giữ nguyên
    }
    if (contentNext < content) {
      return 1; // Đảo vị trí
    }
    return -1;
  });


  renderTable(mangGiam);
};
window.sapxepGiam = sapxepGiam;
