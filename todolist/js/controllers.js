export let renderTable = (list) => {
    

    let contentToDo= "";
    let contentCompleted = "";
    list.forEach((todo) => {

        if (todo.check == 1) {
            let content = /*html*/`
            <li class="task">
            
            <div style="color: green;">
            ${todo.content}
            </div>
            <div class="btn-group">
            <button onclick="deleteTask(${todo.id})" id=""><i class="fa-solid fa-trash-can"></i></button>
            <input onclick = "addToComplete(${todo.id})" type="checkbox" name="" id="checkBox${todo.id}" class="checkBox" checked>
            </div>
            </li>
            `;
            contentCompleted += content;
        }
        else {
            let content = /*html*/`
            <li class="task">
            
            <div>
            ${todo.content}
            </div>
            <div class="btn-group">
            <button onclick="deleteTask(${todo.id})" id=""><i class="fa-solid fa-trash-can"></i></button>
            <input onclick = "addToComplete(${todo.id})" type="checkbox" name="" id="checkBox${todo.id}" class="checkBox">
            </div>
            </li>
            `;
    
            contentToDo += content;
        }




    });
    

    document.getElementById("todo").innerHTML = contentToDo;
    document.getElementById("completed").innerHTML = contentCompleted;

};


export let getNewTask = () => {
    let content = document.getElementById("newTask").value;
    return {
        content,
    };
};
window.getNewTask = getNewTask;


export let findIndex = (id, arr) => {
    for (let index in arr) {
        if (arr[index].id== id) {
            return index;
        }
    }
};



